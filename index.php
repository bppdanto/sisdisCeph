<?php
require 'vendor/autoload.php';
$app = new \Slim\Slim();

define('AWS_KEY', 'FJ164R3L13QTW673UDG0');
define('AWS_SECRET_KEY', '37fyteTEQO2WS0vIsoXmk82AqWhM0CHh5xuFvYNJ');
define('AWS_CANONICAL_ID', 'testuser');
define('AWS_CANONICAL_NAME', 'testuser');
$HOST = 'kurniawan.sisdis.ui.ac.id';

$Connection = new AmazonS3(array(
        'key' => AWS_KEY,
        'secret' => AWS_SECRET_KEY,
        'canonical_id' => AWS_CANONICAL_ID,
        'canonical_name' => AWS_CANONICAL_NAME,
        'certificate_authority'  => TRUE
));

$Connection->set_hostname($HOST);
$Connection->allow_hostname_override(FALSE);
$Connection->enable_path_style();

$app->get('/', function () {
    echo "Hello";
});

// ---------------- SWIFT ----------------

$app->get('/swift/containers', function () {
	require 'swift_init.php';
	curl_setopt_array($ch, array(
        CURLOPT_URL => "kurniawan.sisdis.ui.ac.id:7480/swift/v1",
	));
	$response = curl_exec($ch);
	$containers = explode("\n", $response);
	echo '  Add new container: <form action="container/" method="post">
                  <input type="text" name="dest" placeholder="new container" required> 
                  <input type="submit" value="Add">
                  </form>';
//     echo '  <form action="new_object_swift" method="post" enctype="multipart/form-data">
//     Select image to upload:
//     <input type="file" name="fileToUpload" id="fileToUpload">
//     <input type="text" name="bucket" placeholder="Bucket Name">
//     <input type="submit" value="Upload Image" name="submit">
// </form>';

	foreach ($containers as $container) {
		if (empty($container)) {
			return;
		}
		echo $container;
		echo "<br>";
		echo '<a href="container/'.$container.'"><button>view</button></a>';
		echo '<form action="container/'.$container.'" method="post">
		<input type="hidden" name="_METHOD" value="DELETE" />
		<input type="submit" value="Delete" />
		</form><br>';
	}

});

$app->get('/swift/container/:name', function ($name) {
	require 'swift_init.php';
	curl_setopt_array($ch, array(
        CURLOPT_URL => "kurniawan.sisdis.ui.ac.id:7480/swift/v1/$name",
	));
	$response = curl_exec($ch);
	$objects = explode("\n", $response);
	foreach ($objects as $object) {
		if (empty($object)) {
			return;
		}
		echo $object;
		echo '<a href="'.$name.'/'.$object.'"><button>view</button></a><br>';
		echo 'Copy: <form action="'.$name.'/'.$object.'" method="post">
                  <input type="text" name="dest" placeholder="Filename" required> 
	  Bucket: <input type="text" name="bucket" placeholder="Bucket Name" required>
                  <input type="submit" value="Copy">
                  </form>';
		// echo '<form action="'.$name.'/'.$object.'" method="post">
		// <input type="hidden" name="_METHOD" value="DELETE" />
		// <input type="submit" value="Delete" />
		// </form><br>';
	}
});

$app->post('/swift/container/:name/:object', function ($name, $object) {
  $app = \Slim\Slim::getInstance();
  $bucketdst = $app->request->params('bucket');
  $dst = $app->request->params('dest');
  require 'swift_init.php';
	curl_setopt_array($ch, array(
        CURLOPT_URL => "kurniawan.sisdis.ui.ac.id:7480/swift/v1/$name/$object",
        CURLOPT_CUSTOMREQUEST => "COPY"
	));
	$response = curl_exec($ch);
	var_dump($response);
	// $app->redirect('../'.$name);
});

$app->get('/swift/container/:name/:object', function ($name, $object) {
	require 'swift_init.php';
	curl_setopt_array($ch, array(
        CURLOPT_URL => "kurniawan.sisdis.ui.ac.id:7480/swift/v1/$name/$object",
	));
	$response = curl_exec($ch);
	$base64 = base64_encode($response);
	$ext = explode(".", $object);
  	echo '<img src="data:image/'.$ext[1].';base64,'.$base64.'"/>';
});

$app->delete('/swift/container/:name', function ($name) {
  	$app = \Slim\Slim::getInstance();
	require 'swift_init.php';
	curl_setopt_array($ch, array(
        CURLOPT_URL => "kurniawan.sisdis.ui.ac.id:7480/swift/v1/$name",
        CURLOPT_CUSTOMREQUEST => "DELETE"
	));
	$response = curl_exec($ch);
	$app->redirect('../containers');
});

$app->post('/swift/container/', function () {
  	$app = \Slim\Slim::getInstance();
	require 'swift_init.php';
  	$name = $app->request->params('dest');
	curl_setopt_array($ch, array(
        CURLOPT_URL => "kurniawan.sisdis.ui.ac.id:7480/swift/v1/$name",
        CURLOPT_CUSTOMREQUEST => "PUT"
	));
	$response = curl_exec($ch);
	$app->redirect('../containers');
});

$app->delete('/swift/container/:name/:object', function ($name, $object) {
  	$app = \Slim\Slim::getInstance();
	require 'swift_init.php';
	curl_setopt_array($ch, array(
        CURLOPT_URL => "kurniawan.sisdis.ui.ac.id:7480/swift/v1/$name/$object",
        CURLOPT_CUSTOMREQUEST => "DELETE"
	));
	$response = curl_exec($ch);
	$app->redirect('../'.$name);
});

$app->post('/swift/new_object_swift/', function () {
	$app = \Slim\Slim::getInstance();
	  $bucket = $app->request->params('bucket');
	  $file_resource = fopen($_FILES['fileToUpload']['tmp_name'], 'r');
  
	require 'swift_init.php';
	curl_setopt_array($ch, array(
        CURLOPT_URL => "kurniawan.sisdis.ui.ac.id:7480/swift/v1/$bucket/$file_resource",
        CURLOPT_CUSTOMREQUEST => "PUT"
	));
	$response = curl_exec($ch);
	$app->redirect('../containers');
});

// ---------------------------------------

$app->get('/bucketlist', function () use ($Connection) {
  $ListResponse = $Connection->list_buckets();
  $Buckets = $ListResponse->body->Buckets->Bucket;
  foreach ($Buckets as $Bucket) {
    echo $Bucket->Name . "\t" . $Bucket->CreationDate;
    echo '<a href="deletebucket/'.$Bucket->Name.'"><button>delete</button></a>';
    echo "<br>";
  }
});

$app->post('/create/:name', function ($name) use ($Connection) {
  $app = \Slim\Slim::getInstance();
  shell_exec("python /var/www/html/v2/s3test.py $name");
  $app->redirect('../bucketlist');
});

$app->get('/deletebucket/:name', function ($name) use ($Connection) {
  $app = \Slim\Slim::getInstance();
  $response = $Connection->delete_bucket($name, 1);
  var_dump($response);
  $app->redirect('../bucketlist');
});

$app->get('/uploadview', function(){
  $app = \Slim\Slim::getInstance();
  $app->render('uploadview.html');
});

$app->get('/objectview/:bucket', function($bucket='my-new-bucket') use ($Connection) {
  $app = \Slim\Slim::getInstance();
  $app->render('objectview.html');
  $ObjectsListResponse = $Connection->list_objects($bucket);
  $Objects = $ObjectsListResponse->body->Contents;
  foreach ($Objects as $Object) {
    echo '<img style="max-height: 300px; max-width: 300px;" src=http://kurniawan.sisdis.ui.ac.id:7480/'.$bucket.'/'.$Object->Key.'>';
    echo $Object->Key . "\t" . $Object->Size . "\t" . $Object->LastModified . "\n";
    echo '<form action="../delete/'.$bucket.'/'.$Object->Key.'" method="post">';
    echo "<input type='hidden' name='_METHOD' value='DELETE' />";
    echo "<input type='submit' value='Delete' />";
    echo "</form>";
    echo '<form action="../get/'.$bucket.'/'.$Object->Key.'" method="get">
                  <input type="submit" value="Get">
                  </form>';
    echo '  Copy: <form action="../copy/'.$bucket.'/'.$Object->Key.'" method="post">
                  <input type="text" name="dest" placeholder="Filename" required> 
	  Bucket: <input type="text" name="bucket" placeholder="Bucket Name" required>
                  <input type="submit" value="Copy">
                  </form>';
  }
});

$app->delete('/delete/:bucket/:name', function ($bucket='my-new-bucket', $name) use ($Connection) {
  $app = \Slim\Slim::getInstance();
  $response = $Connection->delete_object($bucket, $name);
  var_dump($response->isOK());
});

$app->get('/get/:bucket/:name', function ($bucket='my-new-bucket', $name) use ($Connection) {
  $app = \Slim\Slim::getInstance();
  $ext = explode(".", $name);
  $response = $Connection->get_object($bucket, $name);
  $base64 = base64_encode($response->body);
  echo '<img src="data:image/'.$ext[1].';base64,'.$base64.'"/>';
});

$app->post('/copy/:bucket/:src', function ($bucket='my-new-bucket', $src) use ($Connection) {
  $app = \Slim\Slim::getInstance();
  $bucketdst = $app->request->params('bucket');
  $dst = $app->request->params('dest');
  $response = $Connection->copy_object(
    array(
      'bucket' => $bucket,
      'filename' => $src
      ), 
    array(
      'bucket' => $bucketdst,
      'filename' => $dst
      ),
    array(
      'acl' => AmazonS3::ACL_PUBLIC
      ));
  var_dump($response->isOK());
});

$app->post('/upload', function () use ($Connection) {
  $app = \Slim\Slim::getInstance();
  $bucket = $app->request->params('bucket');
  $name = $_FILES['fileToUpload']['name'];
  $size = $_FILES['fileToUpload']['size'];
  $file_resource = fopen($_FILES['fileToUpload']['tmp_name'], 'r');
  $response = $Connection->create_object($bucket, $name, array(
    'fileUpload' => $file_resource,
    'length' => $size,
    'acl' => AmazonS3::ACL_PUBLIC
  ));
  var_dump($response->isOK());
});

$app->run();
